export const getFirstLetters = value => {
  return value
    .match(/\b(\w)/g)
    .join('')
    .toLowerCase();
};
