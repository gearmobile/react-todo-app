import React from 'react';
import './TodoInput.css';

// COMPONENTS
import { TodoFilter as Filter } from '../TodoFilter/TodoFilter';

export const TodoInput = ({ onFilter }) => {
  const changeHandler = (e) => {
    onFilter(e.target.value);
  };
  return (
    <div className="d-flex align-items-center justify-content-between mb-3">
      <div className="input-group mr-2">
        <div className="input-group-prepend">
          <span className="input-group-text text-capitalize" id="search">
            filtering
          </span>
        </div>
        <input
          type="text"
          className="form-control"
          onChange={changeHandler}
        />
      </div>
      <Filter />
    </div>
  );
};
