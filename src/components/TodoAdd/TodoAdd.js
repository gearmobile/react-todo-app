import React, { Component } from 'react';

export class TodoAdd extends Component {
  state = {
    input: ''
  };
  onInputChange = e => {
    this.setState({
      input: e.target.value
    });
  };
  onInputSubmit = e => {
    e.preventDefault();
    this.props.onAddItem(this.state.input);
    this.setState({
      input: ''
    });
  };
  render() {
    return (
      <form
        className="d-flex justify-content-between align-items-center"
        onSubmit={this.onInputSubmit}
      >
        <input
          className="form-control flex-shrink-1 text-capitalize"
          type="text"
          name="item"
          placeholder="add something"
          onChange={this.onInputChange}
          value={this.state.input}
        />
        <button
          type="submit"
          className="btn btn-outline-secondary ml-2 flex-grow-1 flex-shrink-0 text-capitalize"
        >
          add task
        </button>
      </form>
    );
  }
}
