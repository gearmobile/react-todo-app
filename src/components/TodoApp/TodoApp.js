import React, { Component } from 'react';
import uuidv4 from 'uuid/v4';

// IMPORT COMPONENTS
import { TodoHeader as Header } from '../TodoHeader/TodoHeader';
import { TodoList as List } from '../TodoList/TodoList';
import { TodoInput as Input } from '../TodoInput/TodoInput';
import { TodoAdd as Add } from '../TodoAdd/TodoAdd';

export class TodoApp extends Component {
  onCreateNewItem = value => {
    return {
      id: uuidv4(),
      label: value.toLowerCase(),
      important: false,
      done: false
    };
  };
  state = {
    items: [
      this.onCreateNewItem(`primo item`),
      this.onCreateNewItem(`secondo item`),
      this.onCreateNewItem(`tetro item`)
    ],
    search: ''
  };
  onFilterItems = value => {
    this.setState({
      search: value
    });
  };
  onRemove = id => {
    this.setState(({ items }) => {
      const newState = items.filter(el => el.id !== id);
      return {
        items: newState
      };
    });
  };
  onAdd = value => {
    this.setState(({ items }) => {
      return {
        items: [...items, this.onCreateNewItem(value)]
      };
    });
  };
  onToggleProperty = (array, id, prop) => {
    return array.map(el => {
      if (el.id === id) {
        el[prop] = !el[prop];
      }
      return el;
    });
  };
  onToggleDone = id => {
    this.setState(({ items }) => {
      return {
        items: this.onToggleProperty(items, id, 'done')
      };
    });
  };
  onToggleImportant = id => {
    this.setState(({ items }) => {
      return {
        items: this.onToggleProperty(items, id, 'important')
      };
    });
  };
  onSearchItems = (array, value) => {
    const pattern = new RegExp(value, 'gi');
    return value.length
      ? [...array].filter(el => pattern.test(el.label))
      : array;
  };
  render() {
    const { items, search } = this.state;
    const onCountDone = items.filter(el => el.done).length;
    const onCountNotDone = items.filter(el => !el.done).length;
    const visibleItems = this.onSearchItems(items, search);
    return (
      <div className="container my-4">
        <Header completeTasks={onCountDone} notCompleteTasks={onCountNotDone} />
        <Input onFilter={this.onFilterItems} />
        <List
          todos={visibleItems}
          onRemoveItem={this.onRemove}
          toggleDone={this.onToggleDone}
          toggleImportant={this.onToggleImportant}
        />
        <Add onAddItem={this.onAdd} />
      </div>
    );
  }
}
