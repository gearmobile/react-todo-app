import React from 'react';

export const TodoDone = ({ complete, notComplete }) => {
  return (
    <span className="font-weight-bold text-secondary">
      {notComplete} not done tasks, {complete} done tasks
    </span>
  );
};
