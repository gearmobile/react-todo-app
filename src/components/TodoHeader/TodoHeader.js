import React from 'react';
import { TodoDone as Done } from './TodoDone/TodoDone';

export const TodoHeader = ({ completeTasks, notCompleteTasks }) => {
  return (
    <header className="d-flex justify-content-between align-items-center">
      <h1 className="mb-3">Todo App</h1>
      <Done complete={completeTasks} notComplete={notCompleteTasks} />
    </header>
  );
};
