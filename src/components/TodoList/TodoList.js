import React from 'react';
import { TodoListItem as ListItem } from './TodoListItem/TodoListItem';

export const TodoList = ({
  todos,
  onRemoveItem,
  toggleDone,
  toggleImportant
}) => {
  const listOfItems = todos.map(todo => {
    const { id, ...itemProps } = todo;
    return (
      <ListItem
        item={itemProps}
        onDelete={() => onRemoveItem(id)}
        onDone={() => toggleDone(id)}
        onImportant={() => toggleImportant(id)}
        key={id}
      />
    );
  });
  return <ul className="list-group mb-4">{listOfItems}</ul>;
};
