import React, { Fragment } from 'react';

export const TodoListItemControl = () => {
  return (
    <Fragment>
      <button className="btn btn-outline-danger mr-1">
        <i className="fa fa-trash" />
      </button>
      <button className="btn btn-outline-info">
        <i className="fa fa-info" />
      </button>
    </Fragment>
  );
};
