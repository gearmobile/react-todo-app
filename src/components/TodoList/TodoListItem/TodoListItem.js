import React from 'react';
import './TodoListItem.css';

export const TodoListItem = ({ item, onDelete, onDone, onImportant }) => {
  const { label, done, important } = item;
  let TodoListItemStyles = `list-group-item d-flex align-items-center justify-content-between todo-list`;
  if (done) {
    TodoListItemStyles += ' done';
  }
  if (important) {
    TodoListItemStyles += ' important';
  }
  const doneHandler = e => {
    e.stopPropagation();
    onDone();
  };
  const importantHandler = e => {
    e.stopPropagation();
    onImportant();
  };
  return (
    <li className={TodoListItemStyles} onClick={doneHandler}>
      <span className="text-capitalize">{label}</span>
      <div>
        <button onClick={onDelete} className="btn btn-outline-danger mr-1">
          <i className="fa fa-trash" />
        </button>
        <button onClick={importantHandler} className="btn btn-outline-info">
          <i className="fa fa-info" />
        </button>
      </div>
    </li>
  );
};
