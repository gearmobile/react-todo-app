import React, { Component } from 'react';

export class TodoFilter extends Component {
  render() {
    return (
      <div className="btn-group mr-2" role="group">
        <button
          type="button"
          className="btn btn-outline-primary text-capitalize"
        >
          all
        </button>
        <button
          type="button"
          className="btn btn-outline-secondary text-capitalize"
        >
          active
        </button>
        <button
          type="button"
          className="btn btn-outline-secondary text-capitalize"
        >
          done
        </button>
      </div>
    );
  }
}
